/**
 * @type {import('next').NextConfig}
 */
const nextConfig = {
  compiler: {
    styledComponents: true,
  },

  // output: "export",
  reactStrictMode: true,
  distDir: "dist",

  images: {
    domains: ["www.ktc.co.th"],
    formats: ["image/webp"],
    // loaderFile: "./my-loader.ts",
  },
  // assetPrefix: "",
  // images: {
  //   domains: ["harries-macbook.local"],
  //   loader: "custom",
  //   loaderFile: "./my-loader.ts",
  // },
};

module.exports = nextConfig;
