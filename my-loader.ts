"use client";
export default function myImageLoader({ src, width, quality }: any) {
  if (src.includes("https://www.ktc.co.th")) {
    return `${src}?w=${width}&q=${quality || 75}`;
  }
  return `https://www.ktc.co.th/pub/media/sites/KTC-PROUD-2023${src}?w=${width}&q=${
    quality || 75
  }`
    .replaceAll(" ", "_")
    .replaceAll(" × ", "_")
    .replaceAll(" ×", "_");
}
