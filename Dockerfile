FROM node:20.2.0
WORKDIR /app
COPY ["package.json", "yarn.lock", "./"]
RUN yarn install --ignore-engines -D --silent
COPY . .
RUN yarn build 
EXPOSE 80
CMD ["yarn", "start"]