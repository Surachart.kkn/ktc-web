"use client";
import { useEffect, useRef, useState } from 'react'
import { useRouter } from 'next/navigation'


export default function Home() {


  const router = useRouter()
  useEffect(() => {
    router.push('/home', { scroll: false })
  });
  return (
    <>

    </>
  )
}

