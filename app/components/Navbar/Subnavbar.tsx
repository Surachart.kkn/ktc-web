"use client";
import styled from "styled-components"

// export const Breadcrumb = styled.div`
// font-style: normal;
// font-weight: 400;
// line-height: normal;
// `

const Subnavbar = () => {
  return (
    <>
      <div style={{ backgroundColor: 'rgba(51, 51, 51, 0.40);' }}>
        <div className="max-w-screen-xl mx-auto p-1" >
          <div className="px-4 text-white text-xs">
            <span className="opacity-60">
              หน้าหลัก
            </span> / สินเชื่อส่วนบุคคล / KTC PROUD
          </div>
        </div>
      </div>
    </>
  )
}

export default Subnavbar
