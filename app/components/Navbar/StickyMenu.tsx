"use client"
import Image from 'next/image'
import { useState } from "react";

import menu from "../../assets/img/menu/AW_KTC_Sub Brand 2.png"


const StickyMenu = (props: any) => {


  const onSendSection = (val: String) => {
    props.section(val);
    setDropdown(!dropdown);
  };

  const [stickyMenu, setStickyMenu] = useState(['รายละเอียดผลิตภัณฑ์', 'ดอกเบี้ยและวงเงิน', 'โปรโมชั่น', 'ขั้นตอนการสมัคร', 'รายละเอียดการสมัคร', 'สนใจสมัคร', 'คำถามที่พบบ่อย', 'บทความ'])

  const [dropdown, setDropdown] = useState<boolean>(false)

  const toggleDropdown = () => {
    setDropdown(!dropdown);
  };

  return (
    <>
      {/* Desktops */}
      <div className="hidden lg:block" style={{
        background: 'rgba(255, 255, 255, 0.90)',
      }}>
        <div className="relative max-w-screen-xl mx-auto p-4">
          <div className="flex justify-between items-center gap-4 ">
            <Image
              src={menu}
              height={32}
              width={115}
              alt="Menu"
            />
            {
              stickyMenu?.map((menu) => (
                <div key={menu} className={`text-gray-700 hover:text-red-800 text-center ${props.underlined === menu && 'border-blue-600 border-b-2'}`} >
                  <button onClick={() => onSendSection(menu)}>
                    {menu}
                  </button>
                </div>
              )
              )
            }
          </div>
        </div>
      </div >

      {/* Mobile */}
      < div className="block lg:hidden" >
        <div className="max-w-screen-xl mx-auto flex items-center" style={{
          background: 'rgba(255, 255, 255, 0.90)',
        }}>
          <button onClick={toggleDropdown} className='w-full px-5 py-3 '>
            <div className="flex justify-between items-center">
              <div className='w-1/2'>
                <Image
                  src={menu}
                  height={32}
                  width={115}
                  alt="Menu"
                />
              </div>
              <div className='w-auto'>
                {
                  !dropdown && (
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                      <path d="M19 8.375L12 15.375L5 8.375" stroke="#707070" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                    </svg>
                  )
                }
                {
                  dropdown && (
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                      <path d="M5 15.625L12 8.625L19 15.625" stroke="#707070" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                    </svg>
                  )
                }
              </div>

            </div>
          </button>

          <div
            style={{ top: '100%', left: '0%', background: 'rgba(255, 255, 255, 0.95)' }}
            className={`z-10 ${dropdown ? 'block' : 'hidden'} font-normaldivide-y rounded-none shadow w-full absolute pt-5`}
          >
            {/* Menu items */}
            <ul className=" text-sm text-gray-700 ">
              {
                stickyMenu?.map((menu) => (
                  <li key={menu} className='mb-3 text-center'>
                    <button className="py-3 text-lg" onClick={() => onSendSection(menu)}>
                      <p className='w-full px-1 hover:border-b-2 hover:border-blue-600'>
                        {menu}
                      </p>
                    </button>
                  </li>
                )
                )
              }
            </ul>

          </div>

        </div>
      </div >
    </>
  )
}

export default StickyMenu

