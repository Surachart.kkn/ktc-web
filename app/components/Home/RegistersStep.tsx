"use client"
import Image from 'next/image'

import step1 from "../../assets/img/stepper/step1.svg"
import step2 from "../../assets/img/stepper/step2.svg"
import step3 from "../../assets/img/stepper/step3.svg"

import fastIcon from "../../assets/img/stepper/fast.png"

const RegistersStep = () => {
  return (
    <>
      <div className="relative z-10">

        {/* Desktops */}
        <div className="hidden md:block">

          <div className="text-2xl text-center fonts-bold text-gray-800">
            3 ขั้นตอนสมัครออนไลน์ง่าย ๆ
          </div>
          <p className="text-base text-center mt-2">
            เพื่อสมัครบัตรกดเงินสด KTC PROUD
          </p>

          <div style={{ height: '40px' }}>
          </div>

          <div className="flex justify-center">
            <ol style={{ width: '69%' }} className="flex items-center text-sm font-medium text-center text-gray-500 dark:text-gray-400 sm:text-base ">

              <li className="flex md:w-full items-center text-white  sm:after:content-[''] after:w-full after:h-1 after:border-dashed after:border-b after:border-red-700 after:border-1 after:hidden sm:after:inline-block after:mx-6 ">
                <div className="rounded-full text-sm flex items-center justify-center bg-red-600" style={{ width: '34px', height: '28px' }}>
                  1
                </div>
              </li>

              <li className="flex md:w-full items-center text-white  sm:after:content-[''] after:w-full after:h-1 after:border-dashed after:border-b after:border-red-700 after:border-1 after:hidden sm:after:inline-block after:mx-6 ">
                <div className="rounded-full text-sm flex items-center justify-center bg-red-600" style={{ width: '34px', height: '28px' }}>
                  2
                </div>
              </li>

              <li className="flex items-cente  text-white">
                <div className="rounded-full text-sm flex items-center justify-center bg-red-600" style={{ width: '28px', height: '28px' }}>
                  3
                </div>
              </li>

            </ol>
          </div>

          <div style={{ height: '40px' }}>
          </div>

          <div className="relative" style={{ width: "100%", height: "300px" }}>

            <div className="flex  items-start justify-around">
              <div className="w-1/3 flex flex-col justify-center px-5 text-center" >
                <div className="flex justify-center" style={{ height: "100px" }}>
                  <Image
                    src={step1}
                    alt="Menu"
                    width={100}
                    height={100}
                    className="h-full"
                  />
                </div>

                <div className="text-md text-gray-800 ">
                  <p className="text-xl   fonts-bold mt-5 mb-3">
                    กรอกข้อมูล
                  </p>
                  เมื่อคุณกรอกข้อมูลส่วนตัว <br />
                  และกดส่งผ่านทางเวปไซต์เรียบร้อยแล้ว <br />
                  เจ้าหน้าที่จะติดต่อกลับภายใน 2 ชม.
                </div>
              </div>

              <div className="w-1/3 flex flex-col justify-center px-5 text-center">
                <div className="flex justify-center" style={{ height: "100px" }}>
                  <Image
                    src={step2}
                    alt="Menu"
                    width={100}
                    height={100}
                    className="h-full"
                  />
                </div>

                <div className="text-md text-gray-800 ">
                  <p className="text-xl   fonts-bold mt-5 mb-3">
                    นัดรับเอกสาร
                  </p>
                  เจ้าหน้าที่จะนัดวันเข้ามารับเอกสารการสมัครบัตรเครดิต <br />
                  ที่บ้านหรือที่ทำงานของคุณ
                </div>
              </div>

              <div className="w-1/3 flex flex-col justify-center px-5 text-center">
                <div className="flex justify-center" style={{ height: "100px" }}>
                  <Image
                    src={step3}
                    alt="Menu"
                    height={100}
                    width={100}
                  />
                </div>

                <div className="text-md text-gray-800 ">
                  <p className="text-xl   fonts-bold mt-5 mb-3">
                    รอผลการอนุมัติ
                  </p>
                  โดยใช้เวลาในการดำเนินการพิจารณา ประมาณ 3-5 <br />
                  วันทำการ นับจากวันที่ใบสมัครเข้าระบบ <br />
                  โดยเอกสารประกอบการสมัครต้องครบถ้วนสมบูรณ์ <br />
                  และได้รับการยืนยันตัวตนจากผู้สมัคร
                </div>
              </div>

            </div>

            <div className="absolute w-full flex justify-evenly" style={{ top: "0%", left: "0%", zIndex: 30 }}>
              <div className="">
                <Image
                  src={fastIcon}
                  alt="Menu"
                  height={120}
                  width={156}
                />
              </div>

              <div className="">
                <Image
                  src={fastIcon}
                  alt="Menu"
                  height={120}
                  width={156}
                />
              </div>

            </div>
          </div>
        </div>

        {/* Mobile */}
        <div className="block md:hidden">
          <div className="flex justify-center flex-col">
            <div className="text-2xl text-center fonts-bold text-gray-800">
              3 ขั้นตอนสมัครออนไลน์ง่าย ๆ
            </div>
            <p className="text-base text-center mt-2">
              เพื่อสมัครบัตรกดเงินสด KTC PROUD
            </p>

          </div>


          <div style={{ height: '40px' }}>
          </div>

          <div className="relative">
            <div className="flex flex-col justify-center gap-7">
              <div className="flex flex-col justify-center px-5 text-center" >
                <div className="flex justify-center" style={{ height: "100px" }}>
                  <Image
                    src={step1}
                    alt="Menu"
                    width={100}
                    height={100}
                    className="h-full"
                  />
                </div>

                <div className="text-md text-gray-800 ">
                  <p className="text-xl   fonts-bold mt-5 mb-3">
                    <span className='italic'> 1. </span>  กรอกข้อมูล
                  </p>
                  เมื่อคุณกรอกข้อมูลส่วนตัว <br />
                  และกดส่งผ่านทางเวปไซต์เรียบร้อยแล้ว <br />
                  เจ้าหน้าที่จะติดต่อกลับภายใน 2 ชม.
                </div>
              </div>

              <div className="flex flex-col justify-center px-5 text-center">
                <div className="flex justify-center" style={{ height: "100px" }}>
                  <Image
                    src={step2}
                    alt="Menu"
                    width={100}
                    height={100}
                    className="h-full"
                  />
                </div>

                <div className="text-md text-gray-800 ">
                  <p className="text-xl   fonts-bold mt-5 mb-3">
                    <span className='italic'> 2. </span> นัดรับเอกสาร
                  </p>
                  เจ้าหน้าที่จะนัดวันเข้ามารับเอกสาร <br />การสมัครบัตรเครดิต
                  ที่บ้านหรือที่ทำงานของคุณ
                </div>
              </div>

              <div className="flex flex-col justify-center px-5 text-center">
                <div className="flex justify-center" style={{ height: "100px" }}>
                  <Image
                    src={step3}
                    alt="Menu"
                    height={100}
                    width={100}
                  />
                </div>

                <div className="text-md text-gray-800 ">
                  <p className="text-xl   fonts-bold mt-5 mb-3">
                    <span className='italic'> 3. </span> รอผลการอนุมัติ
                  </p>
                  โดยใช้เวลาในการดำเนินการพิจารณา<br /> ประมาณ 3-5
                  วันทำการ นับจากวันที่ใบสมัครเข้าระบบ
                  โดยเอกสารประกอบการสมัครต้องครบถ้วนสมบูรณ์
                  และได้รับการยืนยันตัวตนจากผู้สมัคร
                </div>
              </div>

            </div>

          </div>
        </div>


      </div>
    </>
  )
}

export default RegistersStep
