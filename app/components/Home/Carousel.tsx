"use client"
import { Swiper, SwiperSlide } from 'swiper/react';
import { Pagination } from 'swiper/modules';
import 'swiper/css';
import 'swiper/css/pagination';
import Image from 'next/image'

import styles from '../../home/styles.module.css'
import styled from "styled-components"

// Assets
import banner from "../../assets/img/banner/AW_NOV23-014 - Revamp website KTC PROUD_2Desktop1440 × 540.png"
import bannerMobile from "../../assets/img/banner/AW_NOV23-014 - Revamp website KTC PROUD_2Mobile390 ×650.png"
import ribbon from "../../assets/img/props/Red-ribbon.png"

//Styled Components
const Banner = styled.div`
  margin-left: auto;
  margin-right: auto;
  max-width: 1240px;
  max-height: 540px;
`

const Carousel = (props: any) => {

  const onSendSection = (val: String) => {
    props.section(val);

  };

  return (
    <>
      {/* Destops */}
      <div className="hidden md:block">
        <Swiper
          spaceBetween={0}
          slidesPerView={1}
          pagination={{
            clickable: true,
          }}
          modules={[Pagination]}

          onSwiper={(swiper) => console.log(swiper)}
          loop={true}
          style={{ height: '540px' }}
        >
          <SwiperSlide >
            <Image
              src={banner}
              alt="Menu"
              height={544}
            />
            <Banner>
              <div className={styles['banner-text']}>
                <div className="text-5xl mb-4 fonts-bold">
                  บัตรกดเงินสด KTC PROUD
                </div>
                <div className="text-2xl my-2 fonts-bold">
                  อนุมัติปั๊บ รับเงินกู้ก้อนแรกโอนเข้าบัญชีทันที
                </div>
                <div className="text-sm my-2">
                  ขั้นต่ำ 10,000 บาท อัตราดอกเบี้ยและค่าธรรมเนียมการใช้วงเงินสูงสุด 25% ต่อปี <br />
                  เริ่มคำนวณดอกเบี้ยฯ นับตั้งแต่วันที่ได้รับเงินกู้
                </div>
                <div className="flex gap-4 mt-4">
                  <div className="relative">
                    <div className='mt-2' style={{ height: '80px', width: "100%" }}>
                      <Image
                        src={ribbon}
                        alt="Menu"
                      />
                    </div>
                    <div className="absolute" style={{ top: '20px', left: '14px' }}>
                      <p className="text-2xl fonts-bold">
                        พิเศษ
                      </p>
                    </div>
                  </div>
                  <div>
                    <div className="text-4xl fonts-bold">
                      รับดอกเบี้ยฯ
                    </div>
                    <div className="flex">
                      <div >
                        <span className="text-8xl font-bold">
                          19.99
                        </span>
                        <span className="text-6xl font-bold mx-1">
                          %
                        </span>
                        <span className="text-6xl mx-1">
                          ต่อปี
                        </span>
                      </div>
                      <div className="text-6xl mt-6">
                        *
                      </div>
                    </div>
                  </div>
                </div>
                <div className="text-sm my-2">
                  *อัตราดอกเบี้ยฯ พิเศษ สำหรับเงินกู้ก้อนแรก 50,000 บาทขึ้นไป ที่เลือกผ่อนชำระรายเดือน<br />
                  (12 - 60 เดือน) รายการเบิกถอนอื่นจะคิดอัตราดอกเบี้ยฯ ปกติ<br />
                  เฉพาะลูกค้าที่สมัครและได้รับอนุมัติภายใน 31 ธ.ค. 66
                </div>
                <div className='flex justify-between items-end'>
                  <button onClick={() => onSendSection('สนใจสมัคร')} className="bg-red-500 rounded-lg hover:bg-red-600 shadow-md">
                    <div className="relative">
                      <svg width="125" height="48" viewBox="0 0 125 48" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M0 6C0 2.68629 2.68629 0 6 0H51.3424L138 48H6C2.68629 48 0 45.3137 0 42V6Z" fill="url(#paint0_linear_1815_46398)" />
                        <defs>
                          <linearGradient id="paint0_linear_1815_46398" x1="-12.0553" y1="-12.5836" x2="27.9161" y2="79.8361" gradientUnits="userSpaceOnUse">
                            <stop stop-color="white" stop-opacity="0.52117" />
                            <stop offset="0.870601" stop-color="white" stop-opacity="0" />
                          </linearGradient>
                        </defs>
                      </svg>
                      <p className='fonts-bold absolute' style={{ top: '12px', left: '28px' }}>
                        สมัครเลย
                      </p>
                    </div>
                  </button>
                  <div className="text-sm mt-2">
                    กู้เท่าที่จำเป็นและชำระคืนไหว
                  </div>
                </div>

              </div>


            </Banner>
          </SwiperSlide>

        </Swiper>
      </div>

      {/* Mobile */}
      <div className="block md:hidden">
        <Swiper
          spaceBetween={0}
          slidesPerView={1}
          pagination={{
            clickable: true,
          }}
          modules={[Pagination]}

          onSwiper={(swiper) => console.log(swiper)}
          loop={true}
          style={{ height: '650px' }}
        >
          <SwiperSlide >
            <Image
              src={bannerMobile}
              alt="Menu"
            />
            <Banner>
              <div className={styles['banner-text-mobile']}>
                <div className="grid grid-cols-1 gap-4 content-between h-full">
                  <div>
                    <div className="text-2xl fonts-bold">
                      บัตรกดเงินสด KTC PROUD
                    </div>
                    <div className="text-xs my-1 fonts-bol">
                      อนุมัติปั๊บ รับเงินกู้ก้อนแรกโอนเข้าบัญชีทันที
                    </div>
                    <div style={{ fontSize: '8px', lineHeight: '1.5em' }}>
                      ขั้นต่ำ 10,000 บาท อัตราดอกเบี้ยและค่าธรรมเนียมการใช้วงเงินสูงสุด 25% ต่อปี
                      เริ่มคำนวณดอกเบี้ยฯ นับตั้งแต่วันที่ได้รับเงินกู้
                    </div>
                    <div className="flex gap-2 mt-4 text-left">
                      <div className="relative w-auto">
                        <div style={{ height: '60px', width: "100%" }}>
                          <Image
                            src={ribbon}
                            alt="Menu"
                          />
                        </div>
                        <div className="absolute" style={{ top: '10px', left: '6px' }}>
                          <p className="text-xl fonts-bold">
                            พิเศษ
                          </p>
                        </div>
                      </div>
                      <div className="text-xl w-auto">
                        <div className="text-4xl fonts-bold">
                          รับดอกเบี้ยฯ
                        </div>
                        <div className="flex">
                          <div>
                            <span className="text-6xl font-bold">
                              19.99
                            </span>
                            <span className="text-4xl">
                              % ต่อปี
                            </span>
                          </div>
                          <div className="text-2xl">
                            *
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div>
                    <div className="text-left text-gray-200" style={{ fontSize: '11px', lineHeight: '14px' }}>
                      *อัตราดอกเบี้ยฯ พิเศษ สำหรับเงินกู้ก้อนแรก 50,000 บาทขึ้นไป ที่เลือกผ่อนชำระรายเดือน<br />
                      (12-60 เดือน) รายการเบิกถอนอื่นจะคิดอัตราดอกเบี้ยฯ ปกติ<br />
                      เฉพาะลูกค้าที่สมัครและได้รับอนุมัติภายใน 31 ธ.ค. 66 <br />
                      กู็เท่าที่จำป็นและชำคืนไหว
                    </div>

                    <div className="text-right -mt-2">
                      <button onClick={() => onSendSection('สนใจสมัคร')} className="bg-red-500 rounded-lg shadow-lg hover:bg-red-600">
                        <div className="relative">
                          {/* <svg width="125" height="36" viewBox="0 0 125 48" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M0 6C0 2.68629 2.68629 0 6 0H51.3424L138 48H6C2.68629 48 0 45.3137 0 42V6Z" fill="url(#paint0_linear_1815_46398)" />
                            <defs>
                              <linearGradient id="paint0_linear_1815_46398" x1="-12.0553" y1="-12.5836" x2="27.9161" y2="79.8361" gradientUnits="userSpaceOnUse">
                                <stop stop-color="white" stop-opacity="0.52117" />
                                <stop offset="0.870601" stop-color="white" stop-opacity="0" />
                              </linearGradient>
                            </defs>
                          </svg> */}
                          <p className='px-8 py-2 text-sm fonts-bold'>
                            สมัครเลย
                          </p>
                        </div>
                      </button>
                    </div>
                  </div>
                </div>

              </div>
            </Banner>
          </SwiperSlide>
        </Swiper>
      </div >
    </>
  )
}

export default Carousel
