"use client"
import { Swiper, SwiperSlide } from 'swiper/react';
import Image from 'next/image'
import { useState } from 'react';
import Link from 'next/link'
import { Autoplay, Pagination } from 'swiper/modules';

const Promotion = () => {

  const [promotion, setPromotion] = useState([
    {
      url: "https://www.ktc.co.th/pub/media/Pdf/ktc-proud/AW_SEP23-498-Acquisition-19.99.pdf",
      img: "https://www.ktc.co.th/pub/media/KTCPROUD/banner/AW_SEP23-498-_Acquisition_19.99__Hero_Banner_Desktop1440_540_1.webp"
    },
    {
      url: "https://www.ktc.co.th/promotion/cash-card/cash-card/cleardebt66",
      img: "https://www.ktc.co.th/pub/media/Promotion/cash-card/2023/AW_DEC22-319_KTC_PROUD_Clear_Nee_Pee_66_website_banner_PROMO_BANNER_DESKTOP_1448x543.webp"
    },
    {
      url: "https://www.ktc.co.th/promotion/cash-card/cash-card/build-up-ci-online",
      img: "https://www.ktc.co.th/pub/media/Promotion/cash-card/2023/LO1_JUL23-042-Cashback-1500-Website-banner_BN-2.webp"
    },
    {
      url: "https://www.ktc.co.th/promotion/cash-card",
      img: "https://www.ktc.co.th/pub/media/sites/cs/assets/ktc_css/img/proud/LO1_JUN23-045-Product-banner-n.webp"
    }
  ])

  return (
    <>
      {/* Desktop */}
      <div className="relative z-10 hidden md:block">
        <div className="flex justify-between items-end">
          <div className="text-2xl fonts-bold text-gray-800">
            โปรโมชั่น
          </div>
          <div className='text-md'>
            <Link className='hover:text-red-800' href="https://www.ktc.co.th/promotion/cash-card">
              ดูทั้งหมด
            </Link>
          </div>
        </div>
        <div style={{ height: '24px' }}>
        </div>
        <Swiper
          spaceBetween={10}
          slidesPerView={2}
          pagination={{
            clickable: true,
          }}
          modules={[Autoplay, Pagination]}
          onSwiper={(swiper) => console.log(swiper)}
          loop={true}
          style={{ minHeight: '280px' }}
          autoplay={{
            delay: 5000,
            disableOnInteraction: false,
          }}
        >
          {
            promotion.map((promo) => (
              <SwiperSlide className="rounded-lg" key={promo.url}>
                <Link href={promo.url}>
                  <Image
                    src={promo.img}
                    height={270}
                    width={600}
                    alt="Menu"
                    className="rounded-lg"
                    loading="lazy"
                  />
                </Link>
              </SwiperSlide>
            ))
          }
        </Swiper>
      </div >

      {/* Mobile */}
      <div className="relative z-10 block md:hidden" >
        <div className="flex justify-between items-end">
          <div className="text-2xl fonts-bold text-gray-800">
            โปรโมชั่น
          </div>
          <div className='text-md'>
            <Link className='hover:text-red-800' href="https://www.ktc.co.th/article">
              ดูทั้งหมด
            </Link>
          </div>
        </div>
        <div style={{ height: '24px' }}>
        </div>
        <Swiper
          modules={[Autoplay, Pagination]}
          spaceBetween={10}
          slidesPerView={1.1}

          onSwiper={(swiper) => console.log(swiper)}
          loop={true}
          style={{ height: '240px', width: '100%', objectFit: 'cover', objectPosition: 'left' }}
          autoplay={{
            delay: 5000,
            disableOnInteraction: false,
          }}
        >
          {
            promotion.map((promo) => (
              <SwiperSlide className="rounded-lg" key={promo.url}>
                <Link href={promo.url}>
                  <Image
                    src={promo.img}
                    height={270}
                    width={600}
                    alt="Menu"
                    className="rounded-lg object-left"
                    loading="lazy"
                  />
                </Link>
              </SwiperSlide>
            ))
          }
        </Swiper>
      </div>

    </>
  )
}

export default Promotion
