"use client"
import Image from 'next/image'

import styled from "styled-components"
import styles from '../../home/styles.module.css'

import { Swiper, SwiperSlide } from 'swiper/react';
import { useState } from 'react'
import Link from 'next/link'

const BlogText = styled.div`
  height: 40px;
  white-space: normal;
  overflow: hidden;
  text-overflow: ellipsis;
`

const Blog = () => {

  const [article, setArticle] = useState([
    {
      title: "ทำบัตรกดเงินสดใบไหนดี? เปิด 7 เหตุผล ทำไมถึงน่าใช้",
      pic: "https://www.ktc.co.th/pub/media/Article/03/ATM1448x543.webp",
      desc: "การวางแผนการเงินเป็นสิ่งที่คนวัยทำงานควรคำนึงถึง ไม่ว่าจะเป็นเด็กจบใหม่หรือมนุษย์เงินเดือนมือโปรก็ย่อมอยากวางแผนการเงินให้การใช้จ่ายไม่ติดขัด",
      url: "https://www.ktc.co.th/article/knowledge/reason-for-applying-cash-card?amp=1"
    },
    {
      title: "แนะนำบัตรกดเงินสด สมัครง่ายที่สุด รู้ผลอนุมัติไว ไม่ต้องมีคนค้ำ",
      pic: "https://www.ktc.co.th/pub/media/Article/02/cash-card1448x543_1.webp",
      desc: "เมื่อต้องเผชิญปัญหาด้านการเงินไม่ว่าจะเพราะขาดรายได้เนื่องจากเศรษฐกิจซบเซา หรือมีค่าใช้จ่ายฉุกเฉินทำให้ต้องใช้เงินก้อนด่วน",
      url: "https://www.ktc.co.th/article/knowledge/recommend-cash-card-easy-to-apply"
    },
    {
      title: "เปิดช่องทางหาเงินด่วนทันใจ สินเชื่ออนุมัติไวสำหรับคนร้อนเงิน",
      pic: "https://www.ktc.co.th/pub/media/Article/01/quick-cash-loans1448x543.webp",
      desc: "สำหรับใครที่กำลังเผชิญปัญหาขาดสภาพคล่อง หมุนเงินไม่ทันรายจ่าย อยากได้เงินก้อนใหญ่มาช่วยเสริมสภาพคล่องทางการเงินหรือแบ่งเบาภาระหนี้",
      url: "https://www.ktc.co.th/article/knowledge/auto-finance-quick-cash-loan-options"
    }
  ])


  return (
    <>
      <div className="relative">

        <div className="flex justify-between items-center">
          <div className="text-2xl   fonts-bold text-left">
            บทความที่เกี่ยวข้อง
          </div>
          <div className='text-md'>
            <Link className='hover:text-red-800' href="https://www.ktc.co.th/article">
              ดูทั้งหมด
            </Link>
          </div>
        </div>

        <div style={{ height: '40px' }}>
        </div>

        {/* Desktops */}
        <div className='hidden md:block'>
          <div className="flex justify-center items-start gap-6">
            {
              article.map((arts) => (
                <div key={arts.title} className="w-1/3" >
                  <Link href={arts.url}>
                    <Image
                      src={arts.pic}
                      height={217}
                      width={578}
                      alt="icon"
                      className="rounded-lg transition ease-in-out delay-100"
                      style={{ minHeight: '217px', width: '100%', objectFit: 'cover' }}
                    />
                    <div style={{ height: '20px' }}>
                    </div>
                    <div className="text-gray-700">
                      <p className="truncate fonts-bold">
                        {arts.title}
                      </p>
                      <BlogText className="text-sm mt-2">
                        {arts.desc}
                      </BlogText>
                    </div>
                  </Link>
                </div>
              ))
            }
          </div>
        </div>

        {/* Mobile */}
        <div className='block md:hidden'>
          <Swiper
            spaceBetween={6}
            slidesPerView={1.1}

            onSwiper={(swiper) => console.log(swiper)}
            loop={true}
          >
            {
              article.map((arts) => (
                <SwiperSlide key={arts.title} >
                  <Link href={arts.url}>
                    <Image
                      src={arts.pic}
                      height={217}
                      width={578}
                      alt="icon"
                      className="rounded-lg"
                      style={{ minHeight: '180px', width: '100%', objectFit: 'cover' }}
                    />
                    <div style={{ height: '20px' }}>
                    </div>
                    <div className="text-gray-700 text-left">
                      <p className="  fonts-bold">
                        {arts.title}
                      </p>
                      <BlogText className="text-sm mt-2">
                        {arts.desc}
                      </BlogText>
                    </div>
                  </Link>
                </SwiperSlide>
              ))
            }

          </Swiper>
        </div>
      </div >
    </>
  )
}

export default Blog
