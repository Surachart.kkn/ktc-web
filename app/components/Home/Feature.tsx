"use client"
import Image from 'next/image'

import funct_1 from "../../assets/img/function/img_01.png"
import funct_2 from "../../assets/img/function/img_02.png"
import bg from "../../assets/img/function/background.png"
import { useEffect, useState } from 'react';


const Feature = () => {
  const isBrowser = typeof window !== 'undefined';
  const [prevScrollPos, setPrevScrollPos] = useState(isBrowser ? window.scrollY : 0);
  const [up, setUp] = useState(false)

  const [scrollPos, setScrollPos] = useState(isBrowser ? window.scrollY : 0);


  const handleScroll = () => {
    const currentScrollPos = window.scrollY;
    setScrollPos(window.scrollY);

    if (prevScrollPos > currentScrollPos) {
      setUp(true)
    } else if (prevScrollPos < currentScrollPos) {
      setUp(false)
    }

    setPrevScrollPos(currentScrollPos);
  };

  useEffect(() => {
    window.addEventListener('scroll', handleScroll);
    return () => {
      window.removeEventListener('scroll', handleScroll);
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [prevScrollPos]);

  const calculatePanelHeight = (scrollPosition: number) => {
    const minPanelHeight = 0;
    const maxPanelHeight = 600;
    const startAdjustingHeight = 1150;

    // Calculate the scroll distance beyond the startAdjustingHeight
    const scrollDistance = Math.max(0, scrollPosition - startAdjustingHeight);

    // Calculate the interpolated height based on the adjusted scroll distance
    const dynamicHeight = Math.max(minPanelHeight, maxPanelHeight - scrollDistance * 1);

    return `${dynamicHeight}px`;
  };
  return (
    <>
      <div className="relative z-10">
        {/* Destops */}
        <div className='hidden md:block'>
          {/* <section className="tap-to-pay ">
            <div
              className={`p-wrap d-none d-md-block ${up ? 'up' : 'down'}`}
              data-gtm-vis-has-fired67439572_96="1"
            >
              <div className="scroll-sticky">
                <div className="panel _1 relative" data-id="1" style={{ zIndex: '2', height: calculatePanelHeight(scrollPos), width: "510px" }}>

                  <Image
                    src={bg}
                    width={510}
                    height={510}
                    alt="Menu"
                    style={{ flexShrink: 0, position: 'absolute', top: '0%', left: '0%', zIndex: '-1' }}
                  />
                  <Image
                    src={funct_1}
                    width={510}
                    height={510}
                    alt="Menu"
                    style={{ flexShrink: 0, zIndex: '1' }}
                  />

                </div>
                <div className="panel _2 relative" style={{ zIndex: '1', height: '510px', width: "510px" }}
                  data-id="2"
                >
                  <Image
                    src={bg}
                    width={510}
                    height={510}
                    alt="Menu"
                    style={{ flexShrink: 0, position: 'absolute', top: '0%', left: '0%', zIndex: '-1' }}
                  />
                  <Image
                    src={funct_2}
                    width={510}
                    height={200}
                    alt="Menu"
                    style={{ flexShrink: 0, zIndex: '1' }}
                  />
                </div>

              </div>
            </div>
            <div className="text-wrap">
              <div className="panel-text flex">
                <div className="p-4 grid grid-cols-1 gap-4 content-center text-white" style={{ height: '500px' }}>
                  <div className="text-2xl mr-2 fonts-bold text-center md:text-left">
                    <span className=" mr-2 text-amber-300" style={{ fontSize: '28px' }}>
                      ฟังก์ชันครบ
                    </span>
                    จบในบัตรเดียว
                  </div>

                  <div className="grid grid-cols-1 md:grid-cols-2 gap-6 text-center md:text-left" >
                    <div className="mt-4">
                      <p className="text-xl   fonts-bold mb-4">
                        รูด
                      </p>
                      ช้อปสะดวกที่ร้านค้า และช้อปออนไลน์ <br />ได้ทั่วโลก พร้อมรับโปรโมชันพิเศษทั้งปี
                    </div>
                    <div className="mt-4">
                      <p className="text-xl   fonts-bold mb-4">
                        โอน
                      </p>
                      เงินผ่านแอปพลิเคชัน KTC Mobile <br />ตลอด 24 ชม. รับเงินทันที
                    </div>
                    <div className="mt-6">
                      <p className="text-xl   fonts-bold mb-4">
                        กด
                      </p>
                      เงินที่ตู้ ATM ทั่วไทย ฟรีค่าธรรมเนียม
                    </div>
                    <div className="mt-6">
                      <p className="text-xl   fonts-bold mb-4">
                        ผ่อน
                      </p>
                      สินค้า และบริการ ดอกเบี้ย 0% นานสูงสุด <br />24 เดือน (เฉพาะร้านค้าที่ร่วมรายการ)
                    </div>
                  </div>
                </div>
              </div>

              <div className="panel-text flex">
                <div className="p-4 grid grid-cols-1 gap-4 content-center text-white" style={{ height: '500px' }}>
                  <div className="text-2xl md:text-2xl   fonts-bold text-center md:text-left">
                    ขั้นตอนง่ายๆ ในการขอวงเงินเพิ่ม

                  </div>
                  <div className="grid grid-cols-1 md:grid-cols-2 gap-6 text-center md:text-left" >
                    <div className="mt-4">
                      <p className="text-xl   fonts-bold mb-4">
                        ขอวงเงินชั่วคราว*
                      </p>
                      1. ผ่าน  แอป KTC Mobile <br />
                      2. โทร.
                      <span className='text-sm ml-1'>
                        KTC PHONE 02 123 5000 กด 5
                      </span>

                      <p className="text-sm mt-2">
                        หมายเหตุ*เงื่อนไขเป็นไปตามที่บริษัทฯ <br />กำหนด กู้เท่าที่จำเป็น และชำระคืนไหว
                      </p>
                    </div>

                    <div className="mt-4 text-md">

                      <p className="text-xl   fonts-bold mb-4">
                        ขอวงเงินถาวร*
                      </p>

                      กรอกแบบฟอร์มพร้อมแนบเอกสารตัวจริง <br />​และจัดส่งมาที่บริษัทฯ

                      <p className="text-sm mt-2 mb-2">
                        หมายเหตุ*เงื่อนไขเป็นไปตามที่บริษัทฯ<br /> กำหนด​ กู้เท่าที่จำเป็น และชำระคืนไหว
                      </p>

                      <a href="https://www.ktc.co.th/sites/cs/assets/support/download/document/AUG2020-CreditLimit_Form-TH.pdf​" className="text-sm text-amber-300   fonts-bold hover:text-amber-200">
                        <div className="flex gap-1 items-center text-lg justify-center md:justify-start">
                          เพิ่มเติม
                          <svg width="17" height="17" viewBox="0 0 17 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <g id="ep:right">
                              <path id="Vector" d="M12.043 9.41811H2.75C2.61739 9.41811 2.49021 9.47079 2.39645 9.56456C2.30268 9.65833 2.25 9.7855 2.25 9.91811C2.25 10.0507 2.30268 10.1779 2.39645 10.2717C2.49021 10.3654 2.61739 10.4181 2.75 10.4181H12.043L8.396 14.0641C8.30211 14.158 8.24937 14.2853 8.24937 14.4181C8.24937 14.5509 8.30211 14.6782 8.396 14.7721C8.48989 14.866 8.61722 14.9187 8.75 14.9187C8.88278 14.9187 9.01011 14.866 9.104 14.7721L13.604 10.2721C13.6506 10.2257 13.6875 10.1705 13.7127 10.1097C13.7379 10.049 13.7509 9.98388 13.7509 9.91811C13.7509 9.85234 13.7379 9.78722 13.7127 9.72648C13.6875 9.66573 13.6506 9.61056 13.604 9.56411L9.104 5.06411C9.01011 4.97023 8.88278 4.91748 8.75 4.91748C8.61722 4.91748 8.48989 4.97023 8.396 5.06411C8.30211 5.158 8.24937 5.28534 8.24937 5.41811C8.24937 5.55089 8.30211 5.67823 8.396 5.77211L12.043 9.41811Z" fill="#FFD631" stroke="#FFD631" stroke-width="0.5" />
                            </g>
                          </svg>
                        </div>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section> */}

          <div className="grid grid-cols-1 md:grid-cols-2 gap-4">

            <div className="relative " data-id="1" style={{ zIndex: '2', height: "510px", width: "510px" }}>
              <Image
                src={bg}
                width={510}
                height={510}
                alt="Menu"
                style={{ flexShrink: 0, position: 'absolute', top: '0%', left: '0%', zIndex: '-1' }}
              />
              <Image
                src={funct_1}
                width={510}
                height={510}
                alt="Menu"
                style={{ flexShrink: 0, zIndex: '1' }}
              />
            </div>
            <div className="p-4 grid grid-cols-1 gap-4 content-center text-white" style={{ height: '500px' }}>
              <div className="text-2xl mr-2 fonts-bold text-center md:text-left">
                <span className=" mr-2 text-amber-300" style={{ fontSize: '28px' }}>
                  ฟังก์ชันครบ
                </span>
                จบในบัตรเดียว
              </div>

              <div className="grid grid-cols-1 md:grid-cols-2 gap-6 text-center md:text-left" >
                <div className="mt-4">
                  <p className="text-xl   fonts-bold mb-4">
                    รูด
                  </p>
                  ช้อปสะดวกที่ร้านค้า และช้อปออนไลน์ <br />ได้ทั่วโลก พร้อมรับโปรโมชันพิเศษทั้งปี
                </div>
                <div className="mt-4">
                  <p className="text-xl   fonts-bold mb-4">
                    โอน
                  </p>
                  เงินผ่านแอปพลิเคชัน KTC Mobile <br />ตลอด 24 ชม. รับเงินทันที
                </div>
                <div className="mt-6">
                  <p className="text-xl   fonts-bold mb-4">
                    กด
                  </p>
                  เงินที่ตู้ ATM ทั่วไทย ฟรีค่าธรรมเนียม
                </div>
                <div className="mt-6">
                  <p className="text-xl   fonts-bold mb-4">
                    ผ่อน
                  </p>
                  สินค้า และบริการ ดอกเบี้ย 0% นานสูงสุด <br />24 เดือน (เฉพาะร้านค้าที่ร่วมรายการ)
                </div>
              </div>
            </div>
          </div>
          <div style={{ height: "80px" }}>
          </div>
          <div className="grid grid-cols-1 md:grid-cols-2 gap-4">
            <div className="relative " data-id="1" style={{ zIndex: '2', height: "510px", width: "510px" }}>
              <Image
                src={bg}
                width={510}
                height={510}
                alt="Menu"
                style={{ flexShrink: 0, position: 'absolute', top: '0%', left: '0%', zIndex: '-1' }}
              />
              <Image
                src={funct_2}
                width={510}
                height={510}
                alt="Menu"
                style={{ flexShrink: 0, zIndex: '1' }}
              />
            </div>
            <div className="p-4 grid grid-cols-1 gap-4 content-center text-white" style={{ height: '500px' }}>
              <div className="text-2xl md:text-2xl   fonts-bold text-center md:text-left">
                ขั้นตอนง่ายๆ ในการขอวงเงินเพิ่ม

              </div>
              <div className="grid grid-cols-1 md:grid-cols-2 gap-6 text-center md:text-left" >
                <div className="mt-4">
                  <p className="text-xl   fonts-bold mb-4">
                    ขอวงเงินชั่วคราว*
                  </p>
                  1. ผ่าน  แอป KTC Mobile <br />
                  2. โทร.
                  <span className='text-sm ml-1'>
                    KTC PHONE 02 123 5000 กด 5
                  </span>

                  <p className="text-sm mt-2">
                    หมายเหตุ*เงื่อนไขเป็นไปตามที่บริษัทฯ <br />กำหนด กู้เท่าที่จำเป็น และชำระคืนไหว
                  </p>
                </div>

                <div className="mt-4 text-md">

                  <p className="text-xl   fonts-bold mb-4">
                    ขอวงเงินถาวร*
                  </p>

                  กรอกแบบฟอร์มพร้อมแนบเอกสารตัวจริง <br />​และจัดส่งมาที่บริษัทฯ

                  <p className="text-sm mt-2 mb-2">
                    หมายเหตุ*เงื่อนไขเป็นไปตามที่บริษัทฯ<br /> กำหนด​ กู้เท่าที่จำเป็น และชำระคืนไหว
                  </p>

                  <a href="https://www.ktc.co.th/sites/cs/assets/support/download/document/AUG2020-CreditLimit_Form-TH.pdf​" className="text-sm text-amber-300   fonts-bold hover:text-amber-200">
                    <div className="flex gap-1 items-center text-lg justify-center md:justify-start">
                      เพิ่มเติม
                      <svg width="17" height="17" viewBox="0 0 17 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <g id="ep:right">
                          <path id="Vector" d="M12.043 9.41811H2.75C2.61739 9.41811 2.49021 9.47079 2.39645 9.56456C2.30268 9.65833 2.25 9.7855 2.25 9.91811C2.25 10.0507 2.30268 10.1779 2.39645 10.2717C2.49021 10.3654 2.61739 10.4181 2.75 10.4181H12.043L8.396 14.0641C8.30211 14.158 8.24937 14.2853 8.24937 14.4181C8.24937 14.5509 8.30211 14.6782 8.396 14.7721C8.48989 14.866 8.61722 14.9187 8.75 14.9187C8.88278 14.9187 9.01011 14.866 9.104 14.7721L13.604 10.2721C13.6506 10.2257 13.6875 10.1705 13.7127 10.1097C13.7379 10.049 13.7509 9.98388 13.7509 9.91811C13.7509 9.85234 13.7379 9.78722 13.7127 9.72648C13.6875 9.66573 13.6506 9.61056 13.604 9.56411L9.104 5.06411C9.01011 4.97023 8.88278 4.91748 8.75 4.91748C8.61722 4.91748 8.48989 4.97023 8.396 5.06411C8.30211 5.158 8.24937 5.28534 8.24937 5.41811C8.24937 5.55089 8.30211 5.67823 8.396 5.77211L12.043 9.41811Z" fill="#FFD631" stroke="#FFD631" stroke-width="0.5" />
                        </g>
                      </svg>
                    </div>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>

        {/* Mobile */}
        <div className='block md:hidden'>
          {/* Panel 1 */}
          <div className="grid grid-cols-1 md:grid-cols-2 gap-4">
            <div className="relative" >
              <Image
                src={bg}
                width={510}
                height={510}
                alt="Menu"
                style={{ flexShrink: 0, position: 'absolute', top: '0%', left: '0%', zIndex: '-1' }}
              />
              <Image
                src={funct_1}
                width={510}
                height={510}
                alt="Menu"
                style={{ flexShrink: 0 }}
              />
            </div>
            <div className="p-4 grid grid-cols-1 gap-4 content-center text-white">
              <div className=" mr-2   fonts-bold text-center md:text-left" style={{ fontSize: '28px' }}>
                <span className=" md:text-4xl mr-2 text-amber-300">
                  ฟังก์ชันครบ
                </span>
                จบในบัตรเดียว
              </div>

              <div className="grid grid-cols-1 md:grid-cols-2 gap-6 text-center md:text-left" >
                <div className="mt-4">
                  <p className="text-xl   fonts-bold mb-4">
                    รูด
                  </p>
                  ช้อปสะดวกที่ร้านค้า และช้อปออนไลน์ ได้ทั่วโลก พร้อมรับโปรโมชั่นพิเศษทั้งปี
                </div>
                <div className="mt-4">
                  <p className="text-xl   fonts-bold mb-4">
                    โอน
                  </p>
                  เงินผ่านแอพพลิเคชัน KTC Mobile ตลอด 24 ชม. รับเงินทันที
                </div>
                <div className="mt-6">
                  <p className="text-xl   fonts-bold mb-4">
                    กด
                  </p>
                  เงินที่ตู้ ATM ทั่วไทย ฟรีค่าธรรมเนียม
                </div>
                <div className="mt-6">
                  <p className="text-xl   fonts-bold mb-4">
                    ผ่อน
                  </p>
                  สินค้า และบริการ ดอกเบี้ย 0% นานสูงสุด 24 เดือน (เฉพาะร้านค้าที่ร่วมรายการ)
                </div>
              </div>
            </div>
          </div>


          <div style={{ height: "60px" }}>
          </div>

          {/* Panel 2 */}
          <div className="grid grid-cols-1 md:grid-cols-2 gap-4">
            <div className="relative">
              <Image
                src={bg}
                width={510}
                height={510}
                alt="Menu"
                style={{ flexShrink: 0, position: 'absolute', top: '0%', left: '0%', zIndex: '-1' }}
              />
              <Image
                src={funct_2}
                width={510}
                height={510}
                alt="Menu"
                style={{ flexShrink: 0 }}
              />
            </div>
            <div className="p-4 grid grid-cols-1 gap-4 content-center text-white">
              <div className="md:text-2xl   fonts-bold text-center md:text-left" style={{ fontSize: '28px', lineHeight: '1.2em' }}>
                ขั้นตอนง่าย ๆ <br />ในการขอวงเงินเพิ่ม

              </div>
              <div className="grid grid-cols-1 md:grid-cols-2 gap-6 text-center md:text-left" >
                <div className="mt-4">
                  <p className="text-xl   fonts-bold mb-4">
                    ขอวงเงินชั่วคราว*
                  </p>
                  1. ผ่าน  แอป KTC Mobile <br />
                  2. โทร. KTC PHONE 02 123 5000 กด 5​​ <br />
                  <p className="text-sm mt-2">
                    หมายเหตุ * เงื่อนไขเป็นไปตามที่บริษัทฯ <br />กำหนด กู้เท่าที่จำเป็น และชำระคืนไหว
                  </p>
                </div>

                <div className="mt-4 text-md">

                  <p className="text-xl   fonts-bold mb-4">
                    ขอวงเงินถาวร*
                  </p>

                  กรอกแบบฟอร์มพร้อมแนบเอกสารตัวจริง <br />​ และจัดส่งมาที่บริษัทฯ

                  <p className="text-sm mt-2 mb-2">
                    หมายเหตุ * เงื่อนไขเป็นไปตามที่บริษัทฯ <br />กำหนด​ กู้เท่าที่จำเป็น และชำระคืนไหว
                  </p>

                  <a href="https://www.ktc.co.th/sites/cs/assets/support/download/document/AUG2020-CreditLimit_Form-TH.pdf​" className="text-sm text-amber-300   fonts-bold hover:text-amber-200">
                    <div className="flex gap-1 items-center text-lg justify-center md:justify-start">
                      เพิ่มเติม
                      <svg width="17" height="17" viewBox="0 0 17 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <g id="ep:right">
                          <path id="Vector" d="M12.043 9.41811H2.75C2.61739 9.41811 2.49021 9.47079 2.39645 9.56456C2.30268 9.65833 2.25 9.7855 2.25 9.91811C2.25 10.0507 2.30268 10.1779 2.39645 10.2717C2.49021 10.3654 2.61739 10.4181 2.75 10.4181H12.043L8.396 14.0641C8.30211 14.158 8.24937 14.2853 8.24937 14.4181C8.24937 14.5509 8.30211 14.6782 8.396 14.7721C8.48989 14.866 8.61722 14.9187 8.75 14.9187C8.88278 14.9187 9.01011 14.866 9.104 14.7721L13.604 10.2721C13.6506 10.2257 13.6875 10.1705 13.7127 10.1097C13.7379 10.049 13.7509 9.98388 13.7509 9.91811C13.7509 9.85234 13.7379 9.78722 13.7127 9.72648C13.6875 9.66573 13.6506 9.61056 13.604 9.56411L9.104 5.06411C9.01011 4.97023 8.88278 4.91748 8.75 4.91748C8.61722 4.91748 8.48989 4.97023 8.396 5.06411C8.30211 5.158 8.24937 5.28534 8.24937 5.41811C8.24937 5.55089 8.30211 5.67823 8.396 5.77211L12.043 9.41811Z" fill="#FFD631" stroke="#FFD631" stroke-width="0.5" />
                        </g>
                      </svg>
                    </div>

                  </a>
                </div>
              </div>
            </div>
          </div>

          <div style={{ height: "60px" }}>
          </div>
        </div>

      </div>

    </>
  )
}

export default Feature
