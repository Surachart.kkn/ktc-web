"use client"
import { useState } from 'react'

//Components
import CashLimited from '../Calculation/CashLimited'
import Interest from '../Calculation/Interest'

const LoanCredit = () => {
  const [selected, setSelected] = useState<number>(1)

  const onSelected = (val: number) => {
    setSelected(val)
  }

  return (
    <>
      <div className="relative text-center z-10" >
        <div className="text-2xl fonts-bold mb-6 text-gray-700">
          คำนวณวงเงิน และยอดผ่อนชำระ
        </div>

        <div className="flex content-center justify-center">
          <div className='w-1/2 md:w-1/3'>
            <button onClick={() => onSelected(1)} type="button"
              className={selected === 1 ? 'w-full text-gray-800 text-lg fonts-bold py-4 border-b-4 border-sky-600' : 'w-full text-gray-700 text-lg  py-4 border-b border-blue-500'}
            >คำนวณวงเงิน</button>
          </div>
          <div className='w-1/2 md:w-1/3'>
            <button onClick={() => onSelected(2)} type="button"
              className={selected === 2 ? 'w-full text-gray-800 text-lg fonts-bold py-4 border-b-4 border-sky-600' : 'w-full text-gray-700 text-lg  py-4 border-b border-blue-500'}>คำนวณยอดผ่อนชำระ</button>
          </div>
        </div>

        <div style={{ height: '40px' }}>
        </div>

        {/* <div>
          วงเงินเบิกถอนขั้นต่ำ 10,000 บาท และสูงสุดไม่เกิน
        </div> */}

        <div style={{ height: '40px' }}>
        </div>

        {selected === 1 && (
          <CashLimited></CashLimited>
        )}

        {selected === 2 && (
          <Interest></Interest>
        )}

      </div >
    </>
  )
}

export default LoanCredit
