"use client"
import { useState } from "react"

const RegisterDetail = () => {
  const [selected, setSelected] = useState<number>(1)


  return (
    <>
      <div className="relative">
        <div className="text-2xl   fonts-bold mb-6 text-center">
          รายละเอียดการสมัคร
        </div>
        <div className="flex content-center justify-center">
          <div style={{ width: '400px' }}>
            <button onClick={() => setSelected(1)} type="button" className={selected === 1 ? 'w-full text-lg fonts-bold py-4 border-b-4 border-blue-800' : 'w-full text-gray-700 text-lg  py-4 border-b border-blue-500'}>พนักงานบริษัท</button>
          </div>
          <div style={{ width: '400px' }}>
            <button onClick={() => setSelected(2)} type="button" className={selected === 2 ? 'w-full text-lg fonts-bold py-4 border-b-4 border-blue-800' : 'w-full text-gray-700 text-lg  py-4 border-b border-blue-500'}>เจ้าของกิจการ​</button>
          </div>
        </div>

        <div style={{ height: '40px' }}>
        </div>

        <div className={`transition-register flex justify-center ${selected === 1 ? '' : 'hide-register'}`}>
          <div className="text-gray-700 w-full" style={{ maxWidth: '800px' }}>
            <div className="text-lg   fonts-bold mb-2">
              คุณสมบัติผู้สมัคร
            </div>
            <div className="text-md pl-10">
              <ul
                className='list-disc'
              >
                <li>สัญชาติไทย อายุ 20-60 ปี​</li>
                <li>รายได้ขั้นต่ำ 12,000 บาท/เดือน​</li>
                <li>อายุการทำงาน 4 เดือนขึ้นไป​</li>
                <li>เบอร์โทรศัพท์ที่สามารถติดต่อได้ที่อยู่อาศัยหรือที่ทำงาน</li>
              </ul>
            </div>

            <div style={{ height: '32px' }}>
            </div>

            <div className="text-lg   fonts-bold mb-2">
              เอกสารการสมัคร
            </div>
            <div className="text-md pl-10">
              <ul
                className='list-disc'
              >
                <li>สำเนาบัตรประจำตัวประชาชน​</li>
                <li>สำเนาบัตรประจำตัวข้าราชการหรือรัฐวิสาหกิจ และสำเนาทะเบียนบ้าน​</li>
                <li>หนังสือรับรองเงินเดือน / สลิปเงินเดือน เดือนล่าสุด (ฉบับจริง)</li>
                <li>สำเนาบัญชีธนาคารย้อนหลัง 3 เดือน (ชื่อผู้กู้)​​</li>
                <li>สำเนาหน้าแรกของบัญชีธนาคารเพื่อรับเงินโอน (ชื่อผู้กู้)​​​</li>
              </ul>
            </div>
          </div>
        </div>

        <div className={`transition-register flex justify-center ${selected === 2 ? '' : 'hide-register'}`}>
          <div className="text-gray-700 w-full" style={{ maxWidth: '800px' }}>
            <div className="text-lg   fonts-bold mb-2">
              คุณสมบัติผู้สมัคร
            </div>
            <div className="text-md pl-10">

              <ul
                className='list-disc'
              >
                <li>สัญชาติไทย อายุ  20-60 ปี​</li>
                <li> มีรายได้เฉลี่ยขั้นต่ำ 20,000 บาท/เดือน​​</li>
                <li>มีอายุกิจการตั้งแต่ 2 ปีขึ้นไป​​</li>
                <li>มีเบอร์โทรศัพท์ที่สามารถติดต่อได้ที่อยู่อาศัยหรือที่ทำงาน</li>
              </ul>
            </div>

            <div style={{ height: '32px' }}>
            </div>

            <div className="text-lg   fonts-bold mb-2">
              เอกสารการสมัคร
            </div>
            <div className="text-md pl-10">
              <ul
                className='list-disc'
              >
                <li>สำเนาบัตรประจำตัวประชาชน​</li>
                <li>สำเนาหนังสือรับรองจดทะเบียนนิติบุคคล หรือ ทะเบียนการค้า </li>
                <li>สำเนาบัญชีธนาคารย้อนหลัง 6 เดือน (ชื่อผู้กู้)​​​</li>
                <li>สำเนาหน้าแรกของบัญชีธนาคารเพื่อรับเงินโอน (ชื่อผู้กู้)​​​​</li>
              </ul>
            </div>
          </div>
        </div>
      </div>

    </>
  )
}

export default RegisterDetail
