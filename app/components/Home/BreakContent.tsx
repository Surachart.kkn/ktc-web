"use client"
import { useState } from "react";
import Image from 'next/image'
import { Swiper, SwiperSlide } from 'swiper/react';

import icon_1 from "../../assets/img/card/income.svg"
import icon_2 from "../../assets/img/card/atm.svg"
import icon_3 from "../../assets/img/card/withdraw_online.svg"
import icon_4 from "../../assets/img/card/installment.svg"

import styled from "styled-components"

const Card = styled.a`
border-radius: 10px;
border: 1px solid #DFF4FB;
background: linear-gradient(270deg, rgba(233, 241, 252, 0.00) -58.2%, #E9F1FC 13.92%, #FFF 48.92%, rgba(233, 241, 252, 0.89) 89.88%, rgba(233, 241, 252, 0.00) 148.89%);
`

const CardTitle = styled.div`
font-size: 18px;
font-style: normal;
font-weight: 500;
line-height: 150%; 
color:  #262626 !important;
`

const CardText = styled.div`
font-size: 16px;
font-style: normal;
font-weight: 400;
line-height: 150%;
color:  #262626 !important;
`

const BreakContent = () => {
  const [page, setPage] = useState<number>(0)

  const [cardGoods, setCardGoodsu] = useState([
    {
      title: "เงินเดือน  12,000  ก็สมัครได้​",
      text: ["สมัครง่าย ไม่ต้องค้ำ​"],
      icon: icon_1
    },
    {
      title: "รับเงินไว ทันทีที่อนุมัติ",
      text: ["ด้วยบริการเบิกถอนสินเชื่อก้อนแรก​"],
      icon: icon_2
    },
    {
      title: "ไม่มีค่าธรรมเนียม​",
      text: ["ไม่มีค่าธรรมเนียมรายปี และค่าธรรมเนียมการเบิกถอนเงินสด"],
      icon: icon_3
    },
    {
      title: "แบ่งเบาภาระ​",
      text: ["ชำระเต็มจำนวน หรือ ชำระขั้นต่ำ 3% <br /> แต่ไม่ต่ำกว่า  300  บาท"],
      icon: icon_4
    }
  ])

  const onSlideChange = (value: any) => {
    setPage(value.realIndex);
  };
  return (
    <>
      <div className="pb-4 pt-3 relative z-10">
        <div className="max-w-screen-xl mx-auto px-4">
          <div className="  fonts-bold text-2xl text-white mb-8">
            จุดเด่น
          </div>

          {/* Desktops */}
          <div className="hidden md:block">
            <div className="grid grid-cols-4 gap-4">
              {
                cardGoods?.map((card) => (
                  <Card key={card.title} className="block max-w-sm p-5  rounded-lg transition ease-in-out delay-100">
                    <Image
                      src={card.icon}
                      width={80}
                      height={80}
                      alt="icon"
                    />
                    <CardTitle className="mt-4 fonts-bold">
                      {card.title}
                    </CardTitle>
                    <CardText className="mt-3">
                      {card.text.map((text, index) => (
                        text.trim() !== '' && (
                          <p key={index} dangerouslySetInnerHTML={{ __html: text }}></p>
                        )
                      ))}
                    </CardText>
                  </Card>
                )
                )
              }
            </div>
          </div>
        </div>

        {/* Mobile */}
        <div className="block md:hidden">
          <Swiper
            spaceBetween={10}
            slidesPerView={1.2}
            onSlideChange={(val) => onSlideChange(val)}
            onSwiper={(swiper) => console.log(swiper)}
            loop={true}
            centeredSlides={true}
            style={{ height: '240px', width: '100%', objectFit: 'cover', objectPosition: 'left' }}

          >
            {
              cardGoods.map((card) => (
                <SwiperSlide className="rounded-lg" key={card.title}>
                  <div key={card.title} className="p-5 h-full rounded-lg shadow " style={{ background: 'linear-gradient(270deg, rgba(233, 241, 252, 0.00) -58.2%, #E9F1FC 13.92%, #FFF 48.92%, rgba(233, 241, 252, 0.89) 89.88%, rgba(233, 241, 252, 0.00) 148.89%' }}>
                    <div className="flex justify-start gap-3">
                      <div className="w-1/4">
                        <Image
                          width={100}
                          height={100}
                          src={card.icon}
                          alt="icon"
                        />
                      </div>
                      <div className="w-auto fonts-bold flex items-center text-lg text-left">
                        {card.title}
                      </div>
                    </div>
                    <div style={{ height: '24px' }}></div>
                    <div className=" text-left">
                      {card.text.map((text, index) => (
                        text.trim() !== '' && (
                          <p key={index} dangerouslySetInnerHTML={{ __html: text }}></p>
                        )
                      ))}
                    </div>
                  </div>
                </SwiperSlide>
              ))
            }
          </Swiper>

          <div style={{ height: '20px' }}>
          </div>

          <div className="relative">
            <div className="flex items-center justify-center">
              <svg xmlns="http://www.w3.org/2000/svg" width="80" height="8" viewBox="0 0 80 8" fill="none">
                <g filter="url(#filter0_i_9_36594)">
                  <rect width="80" height="8" rx="4" fill="#0F3A73" />
                </g>


                <rect x={page === 1 ? '10' : page === 2 ? '20' : page === 3 ? '30' : '0'} width="50" height="8" rx="4" fill="url(#paint0_linear_9_36594)" />
                <defs>
                  <filter id="filter0_i_9_36594" x="0" y="0" width="82" height="12" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
                    <feFlood flood-opacity="0" result="BackgroundImageFix" />
                    <feBlend mode="normal" in="SourceGraphic" in2="BackgroundImageFix" result="shape" />
                    <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha" />
                    <feOffset dx="2" dy="4" />
                    <feGaussianBlur stdDeviation="2" />
                    <feComposite in2="hardAlpha" operator="arithmetic" k2="-1" k3="1" />
                    <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.25 0" />
                    <feBlend mode="normal" in2="shape" result="effect1_innerShadow_9_36594" />
                  </filter>
                  <linearGradient id="paint0_linear_9_36594" x1="25.7118" y1="0" x2="25.7118" y2="14.6667" gradientUnits="userSpaceOnUse">
                    <stop stop-color="white" />
                    <stop offset="1" stop-color="#AAAAAA" />
                  </linearGradient>
                </defs>
              </svg>

            </div>

          </div>
        </div>

      </div>

    </>
  )
}

export default BreakContent
