"use client"
import { useEffect, useRef, useState } from 'react'

const CashLimited = () => {
  const [occupation, setOccupation] = useState<string>('')
  const [salary, setSaraly] = useState<any>(0)

  const [loan, setLoan] = useState<any>(0)

  const [valid, setValid] = useState<boolean>(false)

  const [checker, setChecker] = useState<boolean>(false)

  const onChangeOccupation = (event: string) => {
    setOccupation(event)
    setDropdownVisible(false)
    setChecker(false)

  };

  const onChangeSalary = (event: any) => {
    if (event.target.value !== " ") {
      updateTextView(event.target.value)

      const integerValue = parseInt(event.target.value.replace(/,/g, ''), 10);

      if (occupation === '') {
        setChecker(true)
      } else if (occupation === 'employee') {
        setChecker(false)
        if (integerValue < 12000) {
          setValid(true)
        } else if (integerValue >= 12000 && integerValue < 30000) {
          setValid(false)
        } else if (integerValue >= 30000 && integerValue <= 10000000) {
          setValid(false)
        }
      } else if (occupation === 'ownbusiness') {
        setChecker(false)
        if (integerValue < 20000) {
          setValid(true)
        } else if (integerValue >= 20000 && integerValue < 30000) {
          setValid(false)
        } else if (integerValue >= 30000 && integerValue <= 10000000) {
          setValid(false)
        }
      }
    }
  }

  const updateTextView = (_str: any) => {
    const num = getNumber(_str);
    if (num === 0) {
      setSaraly('');
    } else {
      setSaraly(num.toLocaleString());
    }
  };

  const getNumber = (_str: any) => {
    const arr = _str.split('');
    const out = [];
    for (let cnt = 0; cnt < arr.length; cnt++) {
      if (!isNaN(arr[cnt])) {
        out.push(arr[cnt]);
      }
    }
    return Number(out.join(''));
  };

  const [isDropdownVisible, setDropdownVisible] = useState(false);

  const toggleDropdown = () => {
    setDropdownVisible(!isDropdownVisible);
  };

  //On Click Outside => Close Toogle
  const areaDetect = useRef(null);

  useEffect(() => {
    if (!toggleDropdown) return;
    function handleClick(event: any) {
      if (areaDetect.current && !(areaDetect.current as HTMLElement).contains(event.target)) {
        setDropdownVisible(false);
      }
    }
    window.addEventListener("click", handleClick);
  });

  const onCalculateLoan = () => {
    const integerValue = parseInt(salary.replace(/,/g, ''), 10);
    if (occupation === '') {
      setChecker(true)
    } else {
      setChecker(false)

      if (occupation === 'employee') {
        let maltiple: any = 0.0

        if (integerValue < 12000) {
          setValid(true)
        } else if (integerValue >= 12000 && integerValue < 30000) {
          setValid(false)
          maltiple = 1.5
        }
        else if (integerValue >= 30000 && integerValue <= 10000000) {
          setValid(false)
          maltiple = 3.00
        }

        const sumLoan = integerValue * maltiple
        const million = 1000000

        if (sumLoan <= 1000000) {
          const formattedPayment = Math.floor(sumLoan).toLocaleString('en-US', { minimumFractionDigits: 2, maximumFractionDigits: 2 });
          setLoan(formattedPayment)

        } else if (sumLoan > 1000000) {
          const formattedPayment = Math.floor(million).toLocaleString('en-US', { minimumFractionDigits: 2, maximumFractionDigits: 2 });
          setLoan(formattedPayment)
        }

      } else if (occupation === 'ownbusiness') {
        let maltiple: any = 0.0

        if (integerValue < 20000) {
          setValid(true)
        } else if (integerValue >= 20000 && integerValue < 30000) {
          setValid(false)
          maltiple = 1.5
        }
        else if (integerValue >= 30000 && integerValue <= 10000000) {
          setValid(false)
          maltiple = 3.00
        }

        const sumLoan = integerValue * maltiple
        const million = 1000000

        if (sumLoan <= 1000000) {
          const formattedPayment = Math.floor(sumLoan).toLocaleString('en-US', { minimumFractionDigits: 2, maximumFractionDigits: 2 });
          setLoan(formattedPayment)

        } else if (sumLoan > 1000000) {
          const formattedPayment = Math.floor(million).toLocaleString('en-US', { minimumFractionDigits: 2, maximumFractionDigits: 2 });
          setLoan(formattedPayment)
        }
      }

    }
  }
  return (
    <>
      <div className="relative content-center justify-center md:flex"  >

        <div className='flex h-[427px] flex-col w-full md:w-1/2 border rounded-lg md:rounded-none md:rounded-l-xl border-gray-100 bg-gray-50 p-5 md:px-[50px] md:py-[40px]' >

          <div className='flex flex-col text-left' ref={areaDetect}>
            <div className="relative py-2">
              <div style={{ position: 'absolute', top: '0%', left: '2%' }} className={`${checker ? 'text-red-500' : 'text-gray-800'} bg-gray-50 px-2 mb-2 text-xs text-gray-800 fonts-bold`}>อาชีพ</div>

              <button
                className={`${checker ? 'border-red-500' : 'border-gray-300'} bg-gray-50 flex w-full border border-gray-300 text-gray-900 text-sm rounded-md item-center justify-between px-4 py-3`}
                onClick={toggleDropdown}
              >
                {occupation === 'employee' && (
                  <p>พนักงานบริษัท</p>
                )}
                {occupation === 'ownbusiness' && (
                  <p>เจ้าของกิจการ</p>
                )}
                {occupation === '' && (
                  <p>กรุณาเลือกอาชีพ</p>
                )}
                <svg
                  className={`w-2.5 h-2.5 ms-2.5 mt-2 transform ${isDropdownVisible ? 'rotate-180' : ''}`}
                  aria-hidden="true"
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 10 8"
                >
                  <path stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="m1 1 4 4 4-4" />
                </svg>

              </button>
              {/* Dropdown menu */}
              <div
                className={`z-10 ${isDropdownVisible ? 'block' : 'hidden'} font-normal bg-white divide-y divide-gray-100 rounded-b-lg shadow w-full  absolute`}
              >
                {/* Menu items */}
                <ul className="py-2 text-sm text-gray-700 ">
                  <li>
                    <button onClick={() => onChangeOccupation('employee')} className="text-left w-full block px-4 py-2 hover:bg-sky-700  hover:text-white">
                      พนักงานบริษัท
                    </button>
                  </li>
                  <li>
                    <button onClick={() => onChangeOccupation('ownbusiness')} className="text-left w-full block px-4 py-2 hover:bg-sky-700   hover:text-white">
                      เจ้าของกิจการ
                    </button>
                  </li>
                </ul>

              </div>
            </div>
            {checker && (
              <span className=' text-xs text-red-500 mx-4'>เลือกอาชีพก่อน</span>
            )}
          </div>

          <div className='flex flex-col text-left mt-6' >

            <div className="relative pt-2">
              <div style={{ position: 'absolute', top: '0%', left: '2%' }} className={`bg-gray-50 px-2 mb-2 text-xs text-gray-800 fonts-bold ${valid && salary !== '' ? 'text-red-500' : 'text-gray-800'}`}>รายได้ต่อเดือน</div>
              <input
                onChange={onChangeSalary}
                maxLength={10}
                value={salary || ""}
                type="text" className={`${valid && salary !== '' ? 'border-red-500 focus:ring-red-500' : 'border-gray-300 focus:ring-gray-500'} bg-gray-50 focus:ring-red-500 placeholder-gray-800 w-full border border-gray-300 text-gray-900 text-sm rounded-md block px-4 py-3`} placeholder="ระบุวงเงิน" required />
              <div className="absolute inset-y-0 end-0 top-2 flex items-center pe-3.5 pointer-events-none text-gray-600 text-sm">
                บาท
              </div>

            </div>
            <label htmlFor="title" className="block mb-2 text-md text-gray-800 h-[24px]">
              {salary === '' || occupation === 'employee' && valid === true && (
                <span className=' text-xs text-red-500 mx-4'>พนักงานบริษัท ต้องมีรายได้ขั้นต่ำ 12,000  บาท ขึ้นไป</span>
              )}
              {salary === '' || occupation === "ownbusiness" && valid === true && (
                <span className=' text-xs text-red-500 mx-4'>เจ้าของกิจการ ต้องมีรายได้ขั้นต่ำ 20,000  บาท ขึ้นไป</span>
              )}

            </label>
          </div>

          <div style={{ height: '40px' }}></div>

          <div className='flex justify-center'>
            <button onClick={onCalculateLoan} disabled={salary === 0 || occupation === '' || valid ? true : false} type="submit"
              className={` ${salary === 0 || occupation === '' || valid ? 'bg-gray-400' : 'bg-sky-700'} text-sm fonts-bold text-gray-50 py-3 rounded-md w-full md:px-10 md:w-auto`} >
              คำนวณ
            </button>
          </div>


          <div className='hidden md:block' style={{ height: '40px' }}></div>

        </div>

        <div className='w-full md:w-1/2 h-[427px] bg-white border mt-6 md:mt-0 rounded-lg md:rounded-none md:rounded-r-xl border-gray-100 overflow-hidden' >
          <div className='credits-card h-full p-5 md:px-[50px] md:py-[40px]'>
            <div className='grid grid-cols-1 gap-4 text-gray-800 content-center h-full text-left'>

              <label htmlFor="title" className="text-sm fonts-bold">ผลคำนวณ</label>

              <div className='text-xl text-right flex justify-between items-end'>
                <div className='text-base'>
                  วงเงินโดยประมาณ
                </div>
                <div>
                  <span className='mx-4 fonts-bold text-blue-600' style={{ fontSize: '1.75rem' }}>
                    {loan.toLocaleString()}
                  </span>
                  บาท
                </div>

              </div>

              <div className='text-xs'>
                วงเงินที่แสดงเป็นเพียงยอดประมาณการเท่านั้น​ การอนุมัติวงเงินสินเชื่อเป็นดุลยพินิจของบริษัทฯ
              </div>

            </div>
          </div>
        </div>
      </div>
    </>
  )
}

export default CashLimited
