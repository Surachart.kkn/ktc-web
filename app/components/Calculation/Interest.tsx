"use client"
import { useEffect, useRef, useState } from 'react'

const Interest = () => {
  const [valid, setValid] = useState<boolean>(false)

  const [checker, setChecker] = useState<boolean>(false)

  // ------------------ Instant Payment Calculations Part ------------------
  const [periodMenu, setPeriodMenu] = useState<number>(0)

  const onChangePeriodMenu = (val: number) => {
    setCash(0)
    setInstallment(0)
    setPayment(0)
    setInterest(0)
    setPeriodMenu(val)
    setMin(false)
    setMax(false)
  }

  const periodMonths = [
    {
      name: '12 เดือน',
      value: 12
    },
    {
      name: '18 เดือน',
      value: 18
    },
    {
      name: '24 เดือน',
      value: 24
    },
    {
      name: '36 เดือน',
      value: 36
    },
    {
      name: '48 เดือน',
      value: 48
    },
    {
      name: '60 เดือน',
      value: 60
    },
  ]

  const [installment, setInstallment] = useState<number>(0)

  const onChangeInstallment = (val: number) => {
    setDropdownVisible(!isDropdownVisible);
    setInstallment(val)
  }

  const [cash, setCash] = useState<any>(0)

  const onChangeCashLimited = (event: any) => {
    // const code = char?.charCodeAt(0);

    console.log(event.target.value)
    if (event.target.value !== " ") {
      updateTextView(event.target.value)

      const integerValue = parseInt(event.target.value.replace(/,/g, ''), 10);

      //Installment payment
      if (integerValue < 10000) {
        setMin(true)
        setMax(false)

      } else if (integerValue >= 10000 && integerValue < 50000) {
        setMin(false)
        setMax(false)

      } else if (integerValue >= 50000 && integerValue <= 1000000) {
        setMin(false)
        setMax(false)

      } else if (integerValue > 1000000) {
        setMin(false)
        setMax(true)
      }
    }

  }

  const updateTextView = (_str: any) => {
    const num = getNumber(_str);
    if (num === 0) {
      setCash('');
    } else {
      setCash(num.toLocaleString());
    }
  };

  const getNumber = (_str: any) => {
    const arr = _str.split('');
    const out = [];
    for (let cnt = 0; cnt < arr.length; cnt++) {
      if (!isNaN(arr[cnt])) {
        out.push(arr[cnt]);
      }
    }
    return Number(out.join(''));
  };

  const [interest, setInterest] = useState<GLfloat>(0)
  const [payment, setPayment] = useState<any>(0)

  const [max, setMax] = useState<boolean>(false)
  const [min, setMin] = useState<boolean>(false)

  const onCalculatePayment = async () => {
    const integerValue = parseInt(cash.replace(/,/g, ''), 10);
    if (periodMenu === 0) {

      //Installment payment
      if (integerValue < 10000) {
        setMin(true)
        setMax(false)

      } else if (integerValue >= 10000 && integerValue < 50000) {
        setMin(false)
        setMax(false)

        setInterest(25.00)

        const monthlyPayment = calculateMonthlyPayment(integerValue, 25.00, installment)

        setPayment(monthlyPayment)

      } else if (integerValue >= 50000 && integerValue <= 1000000) {
        setMin(false)
        setMax(false)

        setInterest(19.99)

        const monthlyPayment = calculateMonthlyPayment(integerValue, 19.99, installment)

        setPayment(monthlyPayment)

      } else if (integerValue > 1000000) {
        setMin(false)
        setMax(true)
      }

    } else if (periodMenu === 1) {

      //Minimum Payment
      if (integerValue < 10000) {
        setMin(true)
        setMax(false)

      } else if (integerValue >= 10000 && integerValue < 50000) {
        setMin(false)
        setMax(false)

        setInterest(25.00)
        const monthlyPayment = calculateMinimumPayment(integerValue, 25.00)

        setPayment(monthlyPayment)

      } else if (integerValue >= 50000 && integerValue <= 1000000) {
        setMin(false)
        setMax(false)

        setInterest(25.00)

        const monthlyPayment = calculateMinimumPayment(integerValue, 25.00)

        setPayment(monthlyPayment)

      } else if (integerValue > 1000000) {
        setMin(false)
        setMax(true)
      }

    }
  }

  // PMT Function Calculations
  function calculateMonthlyPayment(loanAmount: number, annualInterestRate: number, numberOfPayments: number) {

    const monthlyInterestRate = annualInterestRate / 12 / 100;

    const monthlyPayment = (loanAmount * monthlyInterestRate) /
      (1 - Math.pow(1 + monthlyInterestRate, -numberOfPayments));

    const formattedPayment = Math.floor(monthlyPayment).toLocaleString('en-US', { minimumFractionDigits: 2, maximumFractionDigits: 2 });

    return formattedPayment;
  }

  // ------------------ Minimum Payment Calculations Part ------------------

  // Minimun Payments Calculations
  function calculateMinimumPayment(principal: number, anualRate: number) {
    const monthlyInterest: number = (anualRate / 365) * 30

    const estimateInterest: number = principal * (monthlyInterest / 100)

    const estimateMonthlyPayment: number = (principal + estimateInterest) * (3 / 100)

    const formattedPayment = Math.floor(estimateMonthlyPayment).toLocaleString('en-US', { minimumFractionDigits: 2, maximumFractionDigits: 2 });


    return formattedPayment;
  }

  const [isDropdownVisible, setDropdownVisible] = useState(false);

  const toggleDropdown = () => {
    setDropdownVisible(!isDropdownVisible);
  };

  //On Click Outside => Close Toogle
  const areaDetect = useRef(null);

  useEffect(() => {
    if (!toggleDropdown) return;
    function handleClick(event: any) {
      if (areaDetect.current && !(areaDetect.current as HTMLElement).contains(event.target)) {
        setDropdownVisible(false);
      }
    }
    window.addEventListener("click", handleClick);
  });

  return (
    <>
      <div className="relative md:flex content-center justify-center" >

        <div className='flex h-[427px] flex-col md:w-1/2 w-full border rounded-lg md:rounded-none md:rounded-l-xl border-gray-100 bg-gray-50 p-5 md:px-[50px] md:py-[40px]' >

          <div id="button-group" className='relative' >
            <div className='w-full text-left my-1' >
              <label htmlFor="title" className="text-xs fonts-bold text-right">เลือกรับเงินโอน</label>
            </div>
            <div className='flex gap-4 text-xs md:text-base'>
              <button onClick={() => onChangePeriodMenu(0)} className={`${periodMenu === 0 ? 'text-white bg-sky-700 fonts-bold' : 'border-sky-600 text-sky-700'} py-3 w-48 rounded-lg border`}>
                แบบผ่อนชำระรายเดือน
              </button>
              <button onClick={() => onChangePeriodMenu(1)} className={`${periodMenu === 1 ? 'text-white bg-sky-700 fonts-bold' : 'border-sky-600 text-sky-700'} py-3 w-48 rounded-lg border`}>
                แบบชำระขั้นต่ำ
              </button>
            </div>
          </div>

          <div className='hidden md:block' style={{ height: '30px' }}></div>

          <div className='flex flex-col text-left mt-6 md:mt-0'>
            <div className="relative pt-2">
              <div style={{ position: 'absolute', top: '0%', left: '2%' }} className={`bg-gray-50 px-2 mb-2 text-xs text-gray-800 fonts-bold ${cash !== '' && min || max ? 'text-red-500' : 'text-gray-800'}`}>ระบุวงเงินที่ต้องการเบิกถอน</div>
              <input
                onChange={onChangeCashLimited}
                value={cash || ''}
                type="text"
                maxLength={20}
                inputMode="numeric"
                pattern="[0-9\s]{13,19}"
                required
                className={`${cash !== '' && min || max ? 'border-red-500' : 'border-gray-300'} bg-gray-50 w-full border border-gray-300 text-gray-900 text-sm rounded-md  block px-4 py-3 placeholder-gray-800`} placeholder="ระบุวงเงิน" />
              <div className="absolute inset-y-0 end-0 top-2 flex items-center pe-3.5 pointer-events-none text-gray-600 text-sm">
                บาท
              </div>

            </div>
            <label htmlFor="title" className="block mb-2 text-md text-gray-800 h-[24px]">
              {max && (
                <span className=' text-xs text-red-500 mx-4'>ยอดเบิกถอนสูงสุดไม่เกิน 1,000,000 บาท</span>
              )}
              {cash === '' || min && (
                <span className=' text-xs text-red-500 mx-4'>ยอดเบิกถอนขั้นต่ำ 10,000 บาท</span>
              )}
            </label>
          </div>

          {periodMenu === 0 && (
            <>

              <div className='flex flex-col text-left mt-2 ' ref={areaDetect}>
                <div className="relative py-2">
                  <div style={{ position: 'absolute', top: '0%', left: '2%' }} className={`${checker ? 'text-red-500' : 'text-gray-800'} bg-gray-50 px-2 mb-2 text-xs text-gray-800 fonts-bold`}>ต้องการผ่อนกี่เดือน</div>

                  <button
                    className={`${checker ? 'border-red-500' : 'border-gray-300'} bg-gray-50 flex w-full border border-gray-300 text-gray-900 text-sm rounded-md item-center justify-between px-4 py-3`}
                    onClick={toggleDropdown}
                  >

                    {installment === 0 && (
                      <p>ระบุจำนวนเดือน</p>
                    )}
                    {installment !== 0 && (
                      <p>{installment} เดือน</p>
                    )}
                    <svg
                      className={`w-2.5 h-2.5 ms-2.5 mt-2 transform ${isDropdownVisible ? 'rotate-180' : ''}`}
                      aria-hidden="true"
                      xmlns="http://www.w3.org/2000/svg"
                      fill="none"
                      viewBox="0 0 10 8"
                    >
                      <path stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="m1 1 4 4 4-4" />
                    </svg>
                  </button>

                  <div
                    className={`z-10 ${isDropdownVisible ? 'block' : 'hidden'} font-normal bg-white divide-y divide-gray-100 rounded-b-lg shadow w-full  absolute`}
                  >
                    <ul className="py-2 text-sm text-gray-700 ">
                      {
                        periodMonths.map((month) => (
                          <li key={month.name}>
                            <button onClick={() => onChangeInstallment(month.value)} className="text-left w-full block px-4 py-2 hover:bg-sky-700  hover:text-white">
                              {month.name}
                            </button>
                          </li>
                        )
                        )
                      }
                    </ul>
                  </div>
                </div>

              </div>
            </>
          )}

          <div className='flex justify-center mt-4 md:mt-0'>
            {periodMenu === 0 && (
              <button onClick={onCalculatePayment} disabled={installment === 0 || cash === 0 || max || min ? true : false} type="submit"
                className={`${installment === 0 || cash === 0 || max || min ? 'bg-gray-400' : 'bg-sky-700'} text-sm fonts-bold text-gray-50 py-3 rounded-md w-full md:px-10 md:w-auto`} >
                คำนวณ
              </button>
            )
            }
            {periodMenu === 1 && (
              <button onClick={onCalculatePayment} disabled={cash === 0 || max || min ? true : false} type="submit"
                className={`${cash === 0 || max || min ? 'bg-gray-400' : 'bg-sky-700'} text-sm fonts-bold text-gray-50 py-3 rounded-md px-10 w-full md:px-10 md:w-auto`} >
                คำนวณ
              </button>
            )
            }
          </div>



        </div>

        <div className='mt-6 h-[427px] md:mt-0 md:w-1/2 bg-white w-full border rounded-lg md:rounded-none md:rounded-r-xl border-gray-100 overflow-hidden '>
          <div className='credits-card h-full p-5 md:px-[50px] md:py-[40px]'>
            <div className={`${periodMenu === 0 ? 'content-between' : 'content-center'} grid grid-cols-1 gap-4 text-gray-800 h-full text-left`}>
              {periodMenu === 0 && (
                <div className="overflow-x-auto rounded-md">
                  <table className="text-center">
                    <thead className="text-md fonts-bold text-gray-800  bg-amber-300 ">
                      <tr>
                        <th scope="col" className="px-4 md:px-6 py-3" style={{ width: '472px' }}>
                          ยอดรับเงินโอนก้อนแรก
                        </th>
                        <th scope="col" className="px-4 md:px-6 py-3 " style={{ width: '472px' }}>
                          อัตราดอกเบี้ย ต่อปี
                        </th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr className="bg-white border-amber-200 border-b hover:bg-gray-50 text-sm">
                        <th scope="row" className="px-6 py-2 text-gray-900 whitespace-nowrap border-r border-amber-200">
                          50,000 บาทขึ้นไป
                        </th>
                        <td className="px-6 py-2  text-gray-900 whitespace-nowrap">
                          19.99%
                        </td>
                      </tr>
                      <tr className="bg-white  hover:bg-gray-50 text-sm">
                        <th scope="row" className="px-6 py-2  text-gray-900 whitespace-nowrap border-r border-amber-200">
                          ต่ำกว่า 50,000 บาท
                        </th>
                        <td className="px-6 py-2  text-gray-900 whitespace-nowrap">
                          25.00%
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              )}
              <div className='fonts-bold'>
                <label htmlFor="title" className="text-sm">ผ่อน</label>
                <div className='text-sm text-right'>
                  <span className='mx-2 text-blue-600 text-lg' style={{ fontSize: '1.75rem' }}>
                    {payment.toLocaleString()}
                  </span>
                  บาท / เดือน
                </div>
              </div>

              <div className='border-b border-gray-600'>
              </div>

              <div className='fonts-bold'>
                <label htmlFor="title" className="text-sm">อัตราดอกเบี้ย</label>
                <div className='text-sm text-right'>
                  <span className='mx-2 text-blue-600 text-lg'>
                    {interest} %
                  </span>
                  / ปี
                </div>
              </div>


              <div className='text-gray-800' style={{ fontSize: '0.6rem' }}>
                ยอดผ่อนผ่อนชำระที่แสดงเป็นเพียงยอดประมาณการเท่านั้น
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

export default Interest