
"use client"
import styles from './styles.module.css'
import styled from "styled-components"

// Components
import Carousel from "../components/Home/Carousel";
import BreakContent from "../components/Home/BreakContent";
import Feature from "../components/Home/Feature";
import LoanCredit from "../components/Home/LoanCredit";
import Promotion from "../components/Home/Promotion";
import RegistersStep from "../components/Home/RegistersStep";
import RegisterDetail from "../components/Home/RegisterDetail";
import Faq from "../components/Home/Faq";
import Blog from "../components/Home/Blog";
import StickyMenu from '../components/Navbar/StickyMenu';
import { useRef, useEffect, useState } from 'react';

// Styles Components
const Container = styled.div`
  margin-left: auto;
  margin-right: auto;
  width: 100%;
`

const Home = () => {

  const [underlined, setUnderlined] = useState<String>('');

  const section0 = useRef<any>(null);
  const section1 = useRef<any>(null);
  const section2 = useRef<any>(null);
  const section3 = useRef<any>(null);
  const section4 = useRef<any>(null);
  const section5 = useRef<any>(null);
  const section6 = useRef<any>(null);
  const section7 = useRef<any>(null);
  const section8 = useRef<any>(null);

  const onChangeSection = (val: string) => {

    const targetElement = document.getElementById(val);
    targetElement?.scrollIntoView({ behavior: "smooth", block: "start", inline: "center" });

  };

  const observeSection = (sectionRef: any) => {

    const observer = new IntersectionObserver((entries) => {
      entries.forEach((entry) => {
        if (entry.isIntersecting) {
          setUnderlined(entry.target.id)

        }
      });
    }, { threshold: 0.7 });

    if (sectionRef.current) {
      observer.observe(sectionRef.current);
    }

    return () => {
      if (sectionRef.current) {
        observer.unobserve(sectionRef.current);
      }
    };
  };
  useEffect(() => {
    const cleanupFunctions = [
      observeSection(section0),
      observeSection(section1),
      observeSection(section2),
      observeSection(section3),
      observeSection(section4),
      observeSection(section5),
      observeSection(section6),
      observeSection(section7),
      observeSection(section8),
    ];

    // Clean up all observers on component unmount
    return () => {
      cleanupFunctions.forEach(cleanup => cleanup());
    };
  }, []);

  return (
    <>
      <Container>
        {/* Carousel */}
        <Carousel section={onChangeSection}></Carousel>

        {/* Menu Sticky */}
        <div className={styles['sticky-menu']} >
          <StickyMenu section={onChangeSection} underlined={underlined} ></StickyMenu>
        </div>

        {/* Section #1 */}
        <section>
          <div className={`${styles['background-menu']}`}>
            <div className="h-full">

              {/* Content Aventage */}
              <div id="จุดเด่น" ref={section0}>
                <BreakContent></BreakContent>
              </div>

              <div className="max-w-screen-xl mx-auto px-4">

                {/* Feature */}

                <div id="รายละเอียดผลิตภัณฑ์" ref={section1}>
                  <div style={{ height: '120px' }}>
                  </div>
                  <Feature></Feature>
                </div>

                {/* Load and Credit */}

                <div id="ดอกเบี้ยและวงเงิน" ref={section2}>
                  <div style={{ height: '120px' }}>
                  </div>
                  <LoanCredit></LoanCredit>
                </div>

                {/* Promotions  */}

                <div id="โปรโมชั่น" ref={section3}>
                  <div style={{ height: '120px' }}>
                  </div>
                  <Promotion></Promotion>
                </div>

                {/* 3 Step Online Registers  */}

                <div id="ขั้นตอนการสมัคร" ref={section4}>
                  <div style={{ height: '120px' }}>
                  </div>
                  <RegistersStep></RegistersStep>
                </div>

              </div>
            </div>

            {/* Fade Color   */}
            <div className={`${styles['bg-fade']}`} >
            </div>

          </div>
        </section>

        {/* Section #2 */}
        <section>
          <div className="max-w-screen-xl mx-auto px-4 ">

            {/* Register Detail */}
            <div id="รายละเอียดการสมัคร" ref={section5}>
              <div style={{ height: '80px' }}>
              </div>
              <RegisterDetail></RegisterDetail>
            </div>

            {/* Form Register */}
            <div className="relative" id="สนใจสมัคร" ref={section6}>
              <div style={{ height: '120px' }}>
              </div>
              <iframe
                src="https://www.ktc.co.th/apply/webmain-proud"
                title="iframe Example 1"
                className="h-full w-full"
              >
              </iframe>
            </div>


            {/* FAQ */}
            <div id="คำถามที่พบบ่อย" ref={section7}>
              <div style={{ height: '120px' }}>
              </div>
              <Faq></Faq>
            </div>

            {/* Blog */}
            <div id="บทความ" ref={section8}>
              <div style={{ height: '120px' }}>
              </div>
              <Blog></Blog>

            </div>

            <div style={{ height: '240px' }}>
            </div>

          </div>
        </section>


      </Container >
    </>
  )
}

export default Home
