"use client";
import Navbar from "../components/Navbar/Navbar";
import Subnavbar from "../components/Navbar/Subnavbar";

export default function HomeLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <section className='bg-white relative'>
      {/* <Navbar /> */}
      <Subnavbar />

      {children}
    </section>
  )
}